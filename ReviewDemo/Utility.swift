//
//  Utility.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini53 on 3/9/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    
}

var imageDownloadingQueue = NSOperationQueue()
var imageCache = NSCache()

let monthsArray = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
let daysArray = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

//var yearArray = [String]()

func yearArray()->[String] {
    let date = NSDate()
    let calendar = NSCalendar.currentCalendar()
    let components = calendar.components([.Day , .Month , .Year], fromDate: date)
    
    var years = [String]()
    
    for year in 1950..<components.year+1 {
        years.append("\(year)")
    }
    
    return years
}

//var yearArray : (Int, Int) -> [String] = {
//    (a : Int, b : Int) -> [String] in
//
//    let date = NSDate()
//    let calendar = NSCalendar.currentCalendar()
//    let components = calendar.components([.Day , .Month , .Year], fromDate: date)
//
//    var years = [String]()
//
//    for year in a..<components.year {
//        years.append("\(year)")
//    }
//
//    return years
//}

// MARK: Padding View

func setPaddingView(imageName:String, hasImage:Bool)->UIImageView {
    let imageView = UIImageView(frame: CGRectMake(15, 8, 18, 18))
    
    if hasImage{
        let image = UIImage(named: imageName)
        imageView.image = image;
    }
    
    return imageView
}

// MARK: Show Alert with List

func showListAlert(headerNameImageType:HeaderNameImageType , listArray:[String], viewController:UIViewController) {
    
   
}

// MARK: Document Directory Path

func pathToDocumentsDirectory()->String{
    
    let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0]
    return documentsPath as! String
}

func animateView(view:UIView, duration:Double, xValue:CGFloat, yValue:CGFloat){
    
    UIView.animateWithDuration(duration, animations: { () -> Void in
        view.frame = CGRectMake(xValue, yValue, view.frame.size.width, view.frame.size.height)
    }) { (finished) -> Void in
    }
}

// MARK: Show Alert

func showAlert(title:String, message:String, viewController:UIViewController) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
    
    // add the actions (buttons)
    
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
    
    alert.addAction(okAction)
    viewController.presentViewController(alert, animated: true, completion: nil)
    
}


// MARK: Validation

func isValidEmail(testStr:String) -> Bool {
    // println("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(testStr)
}

func validateReferalCode(name : String ) -> Bool {
    
    let emailRegEx = "^[a-z0-9 '.-]{2,20}"
    let range = truncateSpace(name).lowercaseString.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
    let result = range != nil ? true : false
    return result
}

func validateName(name : String ) -> Bool {
    
    //let emailRegEx = "^[a-z'.- ]*"
    let emailRegEx = "^[a-z '.-]{2,20}"
    let range = truncateSpace(name).lowercaseString.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
    let result = range != nil ? true : false
    return result
}

func validFullName(string:String) -> Bool{
    let inValidString = NSCharacterSet(charactersInString: ":~!@#$%^&*()_+{}<?';<>?/.,[]=-0123456789\"")
    if let range = string.rangeOfCharacterFromSet(inValidString)
    {
        print(range)
        return false
    }
    return true
}

func validatePhone(value: String) -> Bool {
    
    let PHONE_REGEX = "^\\d{10}$"
    
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    
    let result =  phoneTest.evaluateWithObject(value)
    
    return result
    
}

// MARK: Delete FolderContent


func createImgDirectory(name:String){
    let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
    let documentsDirectory: AnyObject = paths[0]
    let directoryImgFolderPath = documentsDirectory.stringByAppendingPathComponent(name)
    do {
        try NSFileManager.defaultManager().createDirectoryAtPath(directoryImgFolderPath, withIntermediateDirectories: false, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription);
    }
}

public func deleteContentsOfFolder(filePath:String) {
    // folderURL
    if let folderURL = NSURL(string: filePath)
    {
        // enumerator
        if let enumerator = NSFileManager.defaultManager().enumeratorAtURL(folderURL, includingPropertiesForKeys: nil, options: [], errorHandler: nil)
        {
            // item
            while let item = enumerator.nextObject() {
                // itemURL
                if let itemURL = item as? NSURL {
                    do {
                        try NSFileManager.defaultManager().removeItemAtURL(itemURL)
                    }
                    catch let error as NSError {
                        print("JBSFile Exception: Could not delete item within folder.  \(error)")
                    }
                    catch {
                        print("JBSFile Exception: Could not delete item within folder.")
                    }
                }
            }
        }
    }
}

func truncateSpace(string:String) ->String {
    
    return string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
}

// MARK: Formating

func formatCreditCardNumber( simpleNumber:NSString!,deleteLastChar:Bool)->NSString {
    
    var simpleNumber = simpleNumber
    if simpleNumber.length == 0 {
        return ""
    }
    //    var error:NSError //= NULL;
    let template:NSString = ""
    let regex:NSRegularExpression = try! NSRegularExpression(pattern: "[\\s-\\(\\)]", options: NSRegularExpressionOptions.CaseInsensitive)
    
    simpleNumber = regex.stringByReplacingMatchesInString(simpleNumber as String, options: NSMatchingOptions(), range: NSMakeRange(0, simpleNumber.length), withTemplate: template as String)
    
    
    if(simpleNumber.length>16) {
        // remove last extra chars.
        simpleNumber = simpleNumber.substringToIndex(16)
    }
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = simpleNumber.substringToIndex(simpleNumber.length - 1)
    }
    
    if simpleNumber.length < 5{
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{4})(\\d+)", withString: "$1-$2", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    else if simpleNumber.length < 9{
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{4})(\\d+)", withString: "$1-$2", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    else if simpleNumber.length < 13{
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{4})(\\d{4})(\\d+)", withString: "$1-$2-$3", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    else   {
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{4})(\\d{4})(\\d{4})(\\d+)", withString: "$1-$2-$3-$4", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    
    println(simpleNumber)
    
    return simpleNumber;
}

func formatPhoneNumber(simpleNumber:NSString!,deleteLastChar:Bool)->NSString {
    
    var simpleNumber = simpleNumber
    if simpleNumber.length == 0 {
        return ""
    }
    //    var error:NSError //= NULL;
    let template:NSString = ""
    let regex:NSRegularExpression = try! NSRegularExpression(pattern: "[\\s-\\(\\)]", options: NSRegularExpressionOptions.CaseInsensitive)
    
    simpleNumber = regex.stringByReplacingMatchesInString(simpleNumber as String, options: NSMatchingOptions(), range: NSMakeRange(0, simpleNumber.length), withTemplate: template as String)
    
    
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = simpleNumber.substringToIndex(10)
    }
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = simpleNumber.substringToIndex(simpleNumber.length - 1)
    }
    
    
    if simpleNumber.length < 7{
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{3})(\\d+)", withString: "($1) $2", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    else   {
        simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{3})(\\d{3})(\\d+)", withString: "($1) $2-$3", options: NSStringCompareOptions.RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
    }
    return simpleNumber;
}


func formatString( sender:String)->String {
    return sender.stringByReplacingOccurrencesOfString("/", withString: "").stringByReplacingOccurrencesOfString(" ", withString: "")
}


func getTimeZone() -> String {
    return "\(NSTimeZone.systemTimeZone())".componentsSeparatedByString(" ")[0]
}

func getTimeZoneOffset() -> String {
    return "\(NSTimeInterval(NSTimeZone.localTimeZone().secondsFromGMT))"
}

func formatToCalenderDate(date:NSDate)->String{
    
    let formatter:NSDateFormatter = NSDateFormatter()
    formatter.dateFormat = "EEE\nd\nMMM\n\n"
    //    NSString *stringFromDate = [formatter stringFromDate:date];
    return formatter.stringFromDate(date);
}

func dateStringFromDate(date:NSDate)->String{
    
    let calendar = NSCalendar.currentCalendar()
    let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute, .Second], fromDate: date)
    
    return "\(String(format: "%02d", components.day))-\(String(format: "%02d", components.month))-\(components.year)"
    //    return "\(components.year)-\(components.month)-\(components.day) \(components.hour):\(components.minute):\(components.second)"
}

func getAppointmentDateFormatString(dateString:String)->String{
    
    var formatter:NSDateFormatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone.localTimeZone()
    formatter.locale = NSLocale.systemLocale()
    
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    let date:NSDate = formatter.dateFromString(dateString as String)!
    
    formatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone(name: "GMT")
    formatter.dateFormat = "d MMM, hh:mm a"
    return formatter.stringFromDate(date)
}

func getDateString(dateString:String)->String{
    
    var formatter:NSDateFormatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone.localTimeZone()
    formatter.locale = NSLocale.systemLocale()
    
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    let date:NSDate = formatter.dateFromString(dateString as String)!
    
    formatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone(name: "GMT")
    formatter.dateFormat = "dd-MM-yyyy"
    return formatter.stringFromDate(date)
}


func getDateFromTZ(dateString:String) -> NSDate {
    let formatter:NSDateFormatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone.localTimeZone()
    formatter.locale = NSLocale.systemLocale()
    
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    return formatter.dateFromString(dateString as String)!
}

func getDateFromDateString(dateString:String)->NSDate{
    
    let formatter:NSDateFormatter = NSDateFormatter()
    formatter.timeZone = NSTimeZone.localTimeZone()
    formatter.locale = NSLocale.systemLocale()
    
    formatter.dateFormat = "dd-MM-yyyy"
    
    return formatter.dateFromString(dateString as String)!
}

func getDateFromString(dateString:String)->NSDate{
    
    let formatter:NSDateFormatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    return formatter.dateFromString(dateString as String)!
}

func getMonthDetails(year:NSInteger, month:NSInteger, day:NSInteger) -> (numberOfDays:NSInteger, firstDay:NSInteger) {
    
    let dateComponents = NSDateComponents()
    dateComponents.year = year
    dateComponents.month = month
    dateComponents.day = day
    
    let calendar = NSCalendar.currentCalendar()
    let date = calendar.dateFromComponents(dateComponents)!
    
    let myComponents = calendar.components(.Weekday, fromDate: date)
    
    let range = calendar.rangeOfUnit(.Day, inUnit: .Month, forDate: date)
    let numDays = range.length
    
    return (numDays, myComponents.weekday)
}

func getDayFromString(date:NSDate)->String{
    
    let calendar = NSCalendar.currentCalendar()
    let currentDateComponents = calendar.components([.Year, .Month, .Day, .Hour, .Minute, .Second, .Weekday] , fromDate: date)
    return getDayOfWeek(currentDateComponents.weekday)
}

func getDayOfWeek(weekday:Int) -> String {
    if(weekday == 1) {
        return "Sunday"
    }
    else if(weekday == 2) {
        return "Monday"
    }
    else if(weekday == 3) {
        return "Tuesday"
    }
    else if(weekday == 4) {
        return "Wednesday"
    }
    else if(weekday == 5) {
        return "Thursday"
    }
    else if(weekday == 6) {
        return "Friday"
    }
    else {
        return "Saturday"
    }
    
}

// MARK: Time Conversions

func convertLocalToUtc(dateString: String) ->String {
    
    let dateStringFormatter = NSDateFormatter()
    dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateStringFormatter.timeZone = NSTimeZone.localTimeZone()
    let date = dateStringFormatter.dateFromString(dateString)
    
    let estDf: NSDateFormatter = NSDateFormatter()
    estDf.timeZone = NSTimeZone(name: "GMT")
    estDf.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
    return estDf.stringFromDate(date!)
}

func convertUtcToLocal(dateString: String , getDate:Bool) ->String {
    
    let dateStringFormatter = NSDateFormatter()
    dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateStringFormatter.timeZone = NSTimeZone(name: "UTC")
    let date = dateStringFormatter.dateFromString(dateString)
    
    let estDf: NSDateFormatter = NSDateFormatter()
    
    if getDate {
        estDf.dateFormat = "EEE dd MMM yy"
        estDf.timeZone = NSTimeZone.systemTimeZone()
        return estDf.stringFromDate(date!)
    }
    else {
        estDf.dateFormat = "h:mm a"
        estDf.timeZone = NSTimeZone.systemTimeZone()
        return estDf.stringFromDate(date!)
    }
}

func convertUtcToLocalDay(dateString: String) ->String {
    
    let dateStringFormatter = NSDateFormatter()
    dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    dateStringFormatter.timeZone = NSTimeZone(name: "UTC")
    let date = dateStringFormatter.dateFromString(dateString)
    
    let estDf: NSDateFormatter = NSDateFormatter()
    
    estDf.dateFormat = "dd MMM"
    estDf.timeZone = NSTimeZone.systemTimeZone()
    return estDf.stringFromDate(date!)
}


func getDateFormat(getDate:Bool, date:NSDate) ->String {
    
    let estDf: NSDateFormatter = NSDateFormatter()
    
    if getDate {
        estDf.dateFormat = "dd-MM-yyyy"
        estDf.timeZone = NSTimeZone.systemTimeZone()
        return estDf.stringFromDate(date)
    }
    else {
        estDf.dateFormat = "h:mm a"
        estDf.timeZone = NSTimeZone.systemTimeZone()
        return estDf.stringFromDate(date)
    }
}


func convert24HourTo12Hour(date:String) ->String {
    
    let estDf: NSDateFormatter = NSDateFormatter()
    
    estDf.dateFormat = "HH:mm";
    let newDate = estDf.dateFromString(date)
    
    estDf.dateFormat = "hh:mm a"
    let pmamDateString = estDf.stringFromDate(newDate!)
    
    return pmamDateString
    
}

func convert12HourTo24Hour(date:String) ->String {
    
    let estDf: NSDateFormatter = NSDateFormatter()
    
    estDf.dateFormat = "hh:mm a"; //hh:mm a
    let newDate = estDf.dateFromString(date)
    
    estDf.dateFormat = "HH:mm"
    let pmamDateString = estDf.stringFromDate(newDate!)
    
    return pmamDateString
    
}

func selectedCornerRadius(view:AnyObject) {
    
    let maskPath:UIBezierPath = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [UIRectCorner.BottomRight , UIRectCorner.BottomLeft], cornerRadii: CGSizeMake(5, 5))
    
    let maskLayer:CAShapeLayer = CAShapeLayer()
    maskLayer.frame = view.bounds
    maskLayer.path  = maskPath.CGPath
    //    view.layer.masksToBounds = true
    
    view.layer.mask = maskLayer
}

func addShadow (viewLayer:CALayer, radius:CGFloat, opacity:Float) {
    //    viewLayer.borderColor = UIColor.lightGrayColor().CGColor
    //    viewLayer.borderWidth = 1.5
    
    // drop shadow
    viewLayer.masksToBounds = false
    viewLayer.shadowColor = UIColor.lightGrayColor().CGColor
    viewLayer.shadowOpacity = opacity
    viewLayer.shadowRadius = radius
    viewLayer.shadowOffset = CGSizeMake(2.0, 2.0)
}


func getImageFromUrl(url:String){
}

func removeController(navigationController:UINavigationController, controllerClass:AnyClass) {
    
    let tempVCA = navigationController.viewControllers
    for tempVC: UIViewController in tempVCA {
        if (tempVC.isKindOfClass(controllerClass)) {
            tempVC.removeFromParentViewController()
        }
    }
}

// MARK: - Get indexPath of Table subview

func getIndexpath(sender:AnyObject , tableView:UITableView)->NSIndexPath {
    let indexPath:NSIndexPath = tableView.indexPathForCell(sender.superview?!.superview as! UITableViewCell)!
    return indexPath
}

// MARK: Extensions

extension UIImage {
    class func imageWithColor(color: UIColor) -> UIImage {
        
        let rect: CGRect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContext(rect.size)
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect);
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
extension NSMutableAttributedString{
    
    class func createAttributeText(color:UIColor, font:UIFont, text:NSString,  spacing:Float) ->NSMutableAttributedString{
        
        let attrString = NSMutableAttributedString(string:text as String)
        let attrs = [NSFontAttributeName:font]
        
        attrString.setAttributes(attrs, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSKernAttributeName, value:CGFloat(spacing), range: NSRange(location: 0, length: (text as String).characters.count))
        attrString.addAttribute(NSForegroundColorAttributeName, value:color, range:NSRange(location: 0, length: (text as String).characters.count))
        
        //        var paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.alloc();
        //        paragraphStyle.lineSpacing = 0
        
        
        //        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, count(text as String)))
        
        return attrString
    }
}

extension String {
    
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    var uppercaseFirst: String {
        return first.uppercaseString + String(characters.dropFirst())
    }
}

extension UITextField {
    
    @IBInspectable public var leftSpacer:CGFloat {
        get {
            if let lView = leftView {
                return lView.frame.size.width
            } else {
                return 0
            }
        } set {
            leftViewMode = .Always
            leftView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
        }
    }
}

extension UIImageView {
    
    public func imageFromUrl(imageUrlString: String) {
        
        if let url = NSURL(string: imageUrlString) {
            
            if let cachedImage = imageCache.objectForKey(imageUrlString) as? UIImage {
                self.image = cachedImage
            }
            
            if let cachedImage = imageCache.objectForKey(imageUrlString) as? UIImage {
                self.image = cachedImage
            }
            else {
                // you'll want to initialize the image with some blank image as a placeholder
                
                self.image = UIImage(named: "default-placeholder")
                
                // now download in the image in the background
                
                imageDownloadingQueue.addOperationWithBlock({
                    let request = NSURLRequest(URL: url)
                    
                    let session = NSURLSession.sharedSession()
                    let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                        if error == nil {
                            dispatch_async(dispatch_get_main_queue()) {
                                if(data != nil){
                                    self.image = UIImage(data: data!)
                                    
                                    if self.image != nil {
                                        imageCache.setObject(self.image!, forKey: imageUrlString)
                                    }
                                }
                            }
                        }
                    }
                    task.resume()
                })
            }
        }
    }
    
    //OverLoaded Function
    public func imageFromUrl(imageUrlString: String,defaultImage: String) {
        
        if let url = NSURL(string: imageUrlString) {
            
            if let cachedImage = imageCache.objectForKey(imageUrlString) as? UIImage {
                self.image = cachedImage
            }
            
            if let cachedImage = imageCache.objectForKey(imageUrlString) as? UIImage {
                self.image = cachedImage
            }
            else {
                // you'll want to initialize the image with some blank image as a placeholder
                
                self.image = UIImage(named: defaultImage)
                
                // now download in the image in the background
                
                imageDownloadingQueue.addOperationWithBlock({
                    let request = NSURLRequest(URL: url)
                    
                    let session = NSURLSession.sharedSession()
                    let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                        if error == nil {
                            dispatch_async(dispatch_get_main_queue()) {
                                if(data != nil){
                                    self.image = UIImage(data: data!)
                                    
                                    if self.image != nil {
                                        imageCache.setObject(self.image!, forKey: imageUrlString)
                                    }
                                }
                            }
                        }
                    }
                    task.resume()
                })
            }
        }
    }
}


extension UIButton {
    
    @IBInspectable var OrangeBorder: Bool {
        get {
            return true
        } set {
            //            layer.borderColor = Common.sharedInstance.OrangeColor.CGColor
        }
    }
    
    @IBInspectable var CornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var HighlightedBackground: Bool {
        get {
            return true
        }
        set {
            //            layer.backgroundColor = Common.sharedInstance.OrangeColor.CGColor
            print("highlighted state")
        }
    }
    
    @IBInspectable public var enableExclusiveTouch:Bool {
        get {
            return true
        }
        set {
            exclusiveTouch = newValue
        }
    }
    
}

// MARK: - Clinic World Global Functions


func showSlideOutMenuBtn(viewController: UIViewController, titleName: String){
    NSLog("%@", "kkkkkkk")
    if let vc = viewController.parentViewController?.parentViewController as? SignInViewController{
        NSLog("%@", "ooooooo")
        
        
    }
}

func hideSlideOutMenuBtn(viewController: UIViewController){
    
    if let vc = viewController.parentViewController?.parentViewController as? SignInViewController {
        
    }
    
}
