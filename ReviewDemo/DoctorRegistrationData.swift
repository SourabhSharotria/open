//
//  DoctorRegistrationData.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini53 on 3/14/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit

struct DoctorDegreeKeys {
    static let kServiceCities = "serviceCities"
    static let kCollegeName = "collegeName"
    static let kDegreeName = "degreeName"
    static let kPracticeLocationName = "practiceLocationName"
    static let kPracticeName = "practiceName"
    static let kProofName = "proofName"
}

typealias ReferedDoctorObject = (doctorID:String, doctorName:String, doctorSpeciality:String, doctorClinicAddress:String, doctorClinicID:String)

class DoctorRegistrationData: NSObject {
    
    static let sharedInstance = DoctorRegistrationData()
    
    var isRegisteringDoctor:Bool?
    var isAdmin = false
    
    var fullNameString = String()
    var profileImageOriginal : UIImage?
    var profileImageOriginalString = String()
    var profileImageThumbnail = UIImage()
    var imageUrl = ""
    var profileStatus = ProfileStatus.kNotRegistered
    
    var _id = String()
    var isGetNotification = Bool()
    var isAutoAccept = Bool()
    var interval = Int()
    
    var isEmailVerified = Bool()
    var isPhoneVerified = Bool()
    
    var emailString = String()
    var phoneNumberString = String()
    
    var registrationNumberString = String()
    var registrationDocumentImageString = String()
    
    var ClinicWorldDoctor = String()
    
    var practiceLocationArray = [String]()
    var practiceLocationIDArray = [String]()
    
    var practiceNameArray = [String]()
    var practiceIDArray = [String]()
    var practiceImageArray = [String]()
    var practiceIsOther = [Bool]()
    var practiceIdSpecficDoctorArray = [String]()
    
    var practiceSubSpecialityNameArray = [[String]]()
    var practiceSubSpecialityIDArray = [[String]]()
    var practiceSubSpecialityPriceArray = [[Int]]()
    
    var IDProofNameArray = [String]()
    var IDProofIDArray = [String]()
    
    var consultationChargeString = String()
    
    var photoIDProofImagePathString = String()
    var photoIDProofImage = UIImage()
    
    
    var allIDProofNameArray = [String]()
    var allIDProofIDArray = [String]()
    
    var allCollegeNameArray = [String]()
    var allCollegeIDArray = [String]()
    
    var allDegreeNameArray = [String]()
    var allDegreeIDArray = [String]()
    
    var allPracticeLocationNameArray = [String]()
    var allPracticeLocationIDArray = [String]()
    
    //-------------------------------------- Used for Category test in labroom ------------------------
    
    var cataloguesImgesArray = [String]()
    var allPracticeNameArray = [String]()
    var allPracticeIDArray = [String]()
    
    var allPracticeImageArray = [String]()
    
    var allPracticeSubSpecialityNameArray = [[String]]()
    var allPracticeSubSpecialityIDArray = [[String]]()
    
    var allPracticeSubSpecialityPriceArray = [[Int]]()
    
    var docSubSpecialitySelectionArray = [[Bool]]()
    
    //---------------------------------------- --------------------------- ---------------------------
    
    var primaryRow = Int()
    var shortBio = String()
    var services = String()
    
    
    var selectedAddressFromMap = String()
    var selectedLatitudeFromMap = String()
    var selectedLongitudeFromMap = String()
    
    var selectedPracticeNameToRefer = ""
    var selectedPracticeIdToRefer = ""
    
    func setAllSelectedPractices() {
        
        practiceSubSpecialityNameArray.removeAll()
        practiceSubSpecialityIDArray.removeAll()
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            practiceSubSpecialityPriceArray.removeAll()
        }
        
        
        practiceIDArray.removeAll()
        practiceNameArray.removeAll()
        
        for i in 0..<docSubSpecialitySelectionArray.count {
            
            var selectedSubSpeciality:Bool = false
            
            var subSelNameArray = [String]()
            var subSelIDArray = [String]()
            var subSelPriceArray = [Int]()
            
            for j in 0..<docSubSpecialitySelectionArray[i].count {
                
                if docSubSpecialitySelectionArray[i][j] == true {
                    
                    subSelNameArray.append(allPracticeSubSpecialityNameArray[i][j])
                    subSelIDArray.append(allPracticeSubSpecialityIDArray[i][j])
                    
                    if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
                        subSelPriceArray.append(allPracticeSubSpecialityPriceArray[i][j])
                    }
                    
                    
                    selectedSubSpeciality =  true
                }
            }
            if selectedSubSpeciality {
                practiceNameArray.append(allPracticeNameArray[i])
                practiceIDArray.append(allPracticeIDArray[i])
                
                if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
                    practiceSubSpecialityPriceArray.append(subSelPriceArray)
                }
                
                
                practiceSubSpecialityNameArray.append(subSelNameArray)
                practiceSubSpecialityIDArray.append(subSelIDArray)
            }
        }
    }
    
    func resetData(){
        
        fullNameString = String()
        
        emailString = String()
        phoneNumberString = String()
        registrationNumberString = String()
        registrationDocumentImageString = String()
        imageUrl = ""
        profileStatus = ProfileStatus.kNotRegistered
        
        ClinicWorldDoctor = String()
        
        practiceLocationArray = [String]()
        practiceLocationIDArray = [String]()
        
        practiceNameArray = [String]()
        practiceIDArray = [String]()
        
        practiceSubSpecialityPriceArray = [[Int]]()
        practiceSubSpecialityNameArray = [[String]]()
        practiceSubSpecialityIDArray = [[String]]()
        
        IDProofNameArray = [String]()
        IDProofIDArray = [String]()
        
        consultationChargeString = String()
        
        photoIDProofImagePathString = String()
        
        
        allIDProofNameArray = [String]()
        allIDProofIDArray = [String]()
        
        allCollegeNameArray = [String]()
        allCollegeIDArray = [String]()
        
        allDegreeNameArray = [String]()
        allDegreeIDArray = [String]()
        
        allPracticeLocationNameArray = [String]()
        allPracticeLocationIDArray = [String]()
        
        allPracticeNameArray = [String]()
        allPracticeIDArray = [String]()
        
        allPracticeImageArray = [String]()
        
        allPracticeSubSpecialityNameArray = [[String]]()
        allPracticeSubSpecialityIDArray = [[String]]()
        
        docSubSpecialitySelectionArray = [[Bool]]()
        
        primaryRow = Int()
        shortBio = String()
        services = String()
        
    }
    
    func parseDoctorRegistrationDataObject(temp:NSDictionary){
        
        if let _id = temp["_id"] as? String{
            self._id = _id
        }
        if let isGetNotification = temp["isGetNotification"] as? Bool{
            self.isGetNotification = isGetNotification
        }
        if let isAutoAccept = temp["isAutoAccept"] as? Bool{
            self.isAutoAccept = isAutoAccept
        }
        if let consultationCharges = temp["consultationCharges"] as? Int{
            self.consultationChargeString = String(consultationCharges)
        }
        if let registrationNumberProof = temp["registrationNumberProof"] as? String{
            self.registrationDocumentImageString = registrationNumberProof
        }
        if let registrationNumber = temp["registrationNumber"] as? String{
            self.registrationNumberString = registrationNumber
        }
        if let fullName = temp["fullName"] as? String{
            self.fullNameString = fullName
        }
        if let shortBio = temp["shortBio"] as? String{
            self.shortBio = shortBio
        }
        if let services = temp["services"] as? String{
            self.services = services
        }
        if let interval = temp["interval"] as? Int{
            self.interval = interval
        }
        
        if let photoIdProof = temp["photoIdProof"] as? [String:AnyObject]{
            if let proof = photoIdProof["proof"] as? [String:AnyObject]{
                if let _id = proof["_id"] as? String{
                    self.IDProofIDArray.removeAll()
                    self.IDProofIDArray.append(_id)
                }
                if let proofName = proof["proofName"] as? String{
                    self.IDProofNameArray.removeAll()
                    self.IDProofNameArray.append(proofName)
                }
            }
            if let proofUrl = photoIdProof["proofUrl"] as? String{
                self.photoIDProofImagePathString = proofUrl
            }
        }
        if let servingLocations = temp["servingLocations"] as? [AnyObject]{
            self.practiceLocationIDArray.removeAll()
            self.practiceLocationArray.removeAll()
            for i in 0..<servingLocations.count{
                if let servingLocation = servingLocations[i] as? [String:AnyObject]{
                    if let _id = servingLocation["_id"] as? String{
                        self.practiceLocationIDArray.append(_id)
                    }
                    if let city = servingLocation["city"] as? String{
                        self.practiceLocationArray.append(city)
                    }
                }
            }
        }
        
        
        var areaKey = "practiceAreas"
        var nameKey = "practiceName"
        var practiceKey = "practice"
        
        var specialitiesKey = "specialities"
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            areaKey = "categoryAreas"
            nameKey = "categoryName"
            practiceKey = "category"
            specialitiesKey = "catalogues"
        }
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            self.cataloguesImgesArray.removeAll()
            
            if let cataloguesImgesArray = temp[specialitiesKey] as? [String]{
                self.cataloguesImgesArray = cataloguesImgesArray
            }
        }
        //DoctorRegistrationData.sharedInstance.practiceSubSpecialityPriceArray
        if let practiceAreas = temp[areaKey] as? [AnyObject]{
            self.practiceNameArray.removeAll()
            self.practiceIDArray.removeAll()
            self.practiceIsOther.removeAll()
            self.practiceImageArray.removeAll()
            self.practiceSubSpecialityIDArray.removeAll()
            
            self.practiceIdSpecficDoctorArray.removeAll()
            self.practiceSubSpecialityPriceArray.removeAll()
            
            for i in 0..<practiceAreas.count{
                if let practiceArea = practiceAreas[i] as? [String:AnyObject]{
                    if let _id = practiceArea["_id"] as? String{
                        self.practiceIdSpecficDoctorArray.append(_id)
                    }
                    if let isOther = practiceArea["isOther"] as? Bool{
                        self.practiceIsOther.append(isOther)
                    }
                    if let practice = practiceArea[practiceKey] as? [String:AnyObject]{
                        if let _id = practice["_id"] as? String{
                            self.practiceIDArray.append(_id)
                        }
                        if let practiceName = practice[nameKey] as? String{
                            self.practiceNameArray.append(practiceName)
                        }
                        if let image = practice["image"] as? [String:AnyObject]{
                            if let original = image["original"] as? String{
                                self.practiceImageArray.append(original)
                            }
                            else{
                                self.practiceImageArray.append("")
                            }
                        }
                    }
                    if let specialities = practiceArea[specialitiesKey] as? [String]{
                        self.practiceSubSpecialityNameArray.append(specialities)
                    }
                    
                    if let price = practiceArea["price"] as? NSNumber{
                        self.practiceSubSpecialityPriceArray.append([Int(price)])
                    }
                }
                
                print(practiceSubSpecialityNameArray)
            }
        }
        
        
    }
    
    
}
