//
//  SignUpViewController.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini53 on 3/8/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit
import SafariServices
class SignUpViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var languageTextField: UITextField!
    
    @IBOutlet weak var referalCodeTextField: UITextField!
    
    @IBOutlet weak var profileImageButton: UIButton!
    
    var isShowPassword = false
    var textFieldArray = [UITextField]()
    var imagePath:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
    }

    override func viewWillAppear(animated: Bool) {
        
        DoctorRegistrationData.sharedInstance.resetData()
        
        if (NSFileManager.defaultManager().fileExistsAtPath(pathToDocumentsDirectory().stringByAppendingString("/profilePic"))) {
            DoctorRegistrationData.sharedInstance.imageUrl = pathToDocumentsDirectory().stringByAppendingString("/profilePic")
        }
        
        DoctorRegistrationData.sharedInstance.profileImageOriginal = nil
        profileImageButton.layer.masksToBounds = true;
        profileImageButton.layer.cornerRadius = profileImageButton.frame.size.width / 2;
        profileImageButton.layer.borderWidth = 0.5
        profileImageButton.layer.borderColor = ColorCodes.lightGrayColor.CGColor;
        
        textFieldArray.append(emailTextField)
        textFieldArray.append(phoneNumberTextField)
        textFieldArray.append(passwordTextField)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController!.navigationBar.hidden = false
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        
        //passwordTextField.text = "qwerty"
    }
    
    // MARK: TextField Delegates
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField{
            let phoneString:NSString = "\(textField.text!)\(string)"
            
            // if it's the phone number textfield format it.
            
            if (range.length == 1) {
                // Delete button was hit.. so tell the method to delete the last char.
                phoneNumberTextField.text = formatPhoneNumber(phoneString, deleteLastChar: true) as String
            } else {
                phoneNumberTextField.text = formatPhoneNumber(phoneString, deleteLastChar: false) as String
                
            }
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        let index = (textFieldArray).indexOf(textField)
        
        if index == textFieldArray.count-1 {
            textField.resignFirstResponder()
        }
        else {
            (textFieldArray[index!+1]).becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == emailTextField {
            textField.text = truncateSpace(textField.text!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func skipClicked(sender: AnyObject) {
        
        

    }    
    
    @IBAction func nextClicked(sender: AnyObject) {
        
        self.view.endEditing(true)

        if isValidEmail(emailTextField.text!)==false {
            
            showAlert("", message: "Email is not valid", viewController: self)
        }
        else if phoneNumberTextField.text!.characters.count != 14 {
            showAlert("", message: "Please enter valid mobile number.", viewController: self)
        }
        else if (passwordTextField.text!.characters.count==0) {
            showAlert("", message: "Please enter password.", viewController: self)
        }
        else {
            
            var phoneNumber:NSString = (phoneNumberTextField.text)!
            phoneNumber = (((phoneNumber.stringByReplacingOccurrencesOfString("-", withString: "")).stringByReplacingOccurrencesOfString("(", withString: "")).stringByReplacingOccurrencesOfString(")", withString: "")).stringByReplacingOccurrencesOfString(" ", withString: "")
            var userType = String()
            if((DoctorRegistrationData.sharedInstance.isRegisteringDoctor) == true){
                userType = "DOCTOR"
            }else{
                userType = "LAB_ROOM"
            }
            let postParamDictionary = [
                "email":emailTextField.text!,
                "password":passwordTextField.text!,
                "phoneNumber":phoneNumber,
                "deviceType":Keys.kDeviceType as String,
                "deviceName":Keys.kDeviceName as String,
                "deviceToken":NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken as String)!,
                "appVersion":Keys.kAppVersion as String,
                "timezone":getTimeZone(),
                "timezoneOffset" : getTimeZoneOffset(),
                "userType" : userType
            ]
            
            var imageData = [NSData]()
            var keyName = [String]()
            if (NSFileManager.defaultManager().fileExistsAtPath((imagePath) as String) ){
                 imageData = [NSData(contentsOfFile: imagePath)!]
                keyName = ["profilePicture"]
            }
            
            print(postParamDictionary)
            
         
//            let postParamDictionary = [
//                "email":"akhilesh.gandotra@mail.click-labs.com",
//                "password":"qwerty",
//                "phoneNumber":"7508162163",
//                "deviceType":Keys.kDeviceType as String,
//                "deviceName":Keys.kDeviceName as String,
//                "deviceToken":NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken as String)!,
//                "appVersion":Keys.kAppVersion as String,
//                "timezone":getTimeZone(),
//                "timezoneOffset" : getTimeZoneOffset()]
            
             
        }
    }
    
    // MARK: - Button Actions
    
    @IBAction func profileImageClicked(sender: AnyObject) {
        
        
    }
    
    @IBAction func termsConditionClicked(sender: AnyObject) {
        let svc = SFSafariViewController(URL: NSURL(string: "https://www.google.co.in")!)
        self.presentViewController(svc, animated: true, completion: nil)
    }
    
    
    @IBAction func showPasswordClicked(sender: UIButton) {
        if(passwordTextField.text != ""){
            self.isShowPassword = !isShowPassword
            if(isShowPassword){
                sender.setTitle("Hide", forState: .Normal)
                sender.setTitleColor(ColorCodes.floatingPlaceholderColor, forState: .Normal)
                passwordTextField.secureTextEntry = false
            }
            else{
                sender.setTitle("Show", forState: .Normal)
                sender.setTitleColor(ColorCodes.greenholderColor, forState: .Normal)
                passwordTextField.secureTextEntry = true
            }
        }
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    // MARK: - Image Notificaton Handler
    
    func imageSelectedSuccesfully(filePath:String){
        imagePath = filePath
        profileImageButton.setImage(UIImage(contentsOfFile: filePath), forState: UIControlState.Normal)
        
        DoctorRegistrationData.sharedInstance.profileImageOriginalString = filePath
//        DoctorRegistrationData.sharedInstance.profileImageOriginal = UIImage(contentsOfFile: filePath)
    }
    
    // MARK: - Server File Delgate Actions
    
    func callAlertView(title:String, message:String) {
        dispatch_async(dispatch_get_main_queue(), {
            showAlert("", message: message, viewController: self)
        })
    }
    

}
